/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chase.view;

/**
 *
 * @author Levente Daradics
 */
public enum KanaTypes {
    HIRAGANA("Hiragana"),
    KATAKANA("Katakana")
    ;
    
    private final String text;
    
    private KanaTypes(final String text)  {
        this.text = text;
    }
    
    @Override
    public String toString()    {
        return text;
    }
}
