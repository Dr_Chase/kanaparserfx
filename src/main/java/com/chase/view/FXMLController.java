package com.chase.view;

import com.chase.katakana.RomajiLettersKat;
import com.chase.katakana.KatakanaLetters;
import com.chase.hiragana.RomajiLettersHir;
import com.chase.hiragana.HiraganaLetters;
import com.sun.javafx.collections.ObservableListWrapper;
import java.net.URL;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

public class FXMLController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private TextArea jpnIOTextArea;
    
    @FXML
    private TextArea latinIOTextArea;
    
    @FXML
    private ChoiceBox choiceBoxJpn;
    
    @FXML
    private ChoiceBox choiceBoxLatin;
    
    @FXML
    private Button btnTranslateFromJpn;
    
    @FXML
    private Button btnTranslateFromLatin;
    
    private EnumSet<KanaTypes> kanaTypes;
    
    private KanaTypes fromJpnChoiceBoxEnum = KanaTypes.HIRAGANA;
    
    private KanaTypes fromLatinChoiceBoxEnum = KanaTypes.HIRAGANA;
    
    private HiraganaLetters hiraganaParser;
    
    private KatakanaLetters katakanaParser;
    
    private RomajiLettersHir romajiToHiraganaParser;
    
    private RomajiLettersKat romajiToKatakanaParser;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        String inputText = jpnIOTextArea.getText();
        String outputText;
//        Character hiraganaCharacter = jpnIOTextArea.getText().charAt(0);
//        outputText = String.valueOf(HiraganaLettersOlder.mapHiraganaKeyRomanjiValue.get(hiraganaCharacter));
        HiraganaLetters parser = HiraganaLetters.getInstance();
        //outputText = HiraganaLettersOlder.parseHiraganaString(inputText);
        outputText = parser.parseHiraganaString(inputText);
        label.setText(outputText);
    }
    
    @FXML
    private void translateToLatin()  {
        String latin;
        switch (fromJpnChoiceBoxEnum)   {
            
            case HIRAGANA:
                String hiragana = jpnIOTextArea.getText();
                latin = hiraganaParser.parseHiraganaString(hiragana);
                latinIOTextArea.setText(latin);
                break;
            case KATAKANA:
                String katakana = jpnIOTextArea.getText();
                latin = katakanaParser.parseKatakanaString(katakana);
                latinIOTextArea.setText(latin);
                break;
        }
    }
    
    @FXML
    private void translateToJPN()  {
        String jpnText;
        switch  (fromLatinChoiceBoxEnum)    {
            case HIRAGANA:
                String hiragana = latinIOTextArea.getText();
                jpnText = romajiToHiraganaParser.convertToHiragana(hiragana);
                jpnIOTextArea.setText(jpnText);
                break;
            case KATAKANA:
                String katakana = latinIOTextArea.getText();
                jpnText = romajiToKatakanaParser.parseKatakanaString(katakana);
                jpnIOTextArea.setText(jpnText);
                break;
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        hiraganaParser = HiraganaLetters.getInstance();
        
        katakanaParser = KatakanaLetters.getInstance();
        
        romajiToHiraganaParser = RomajiLettersHir.getInstance();
        
        romajiToKatakanaParser = RomajiLettersKat.getInstance();
        
        List<KanaTypes> jpnChbValues = new ArrayList<>(EnumSet.allOf(KanaTypes.class));
        
        ObservableList<KanaTypes> jpnChbValuesObservable = new ObservableListWrapper<>(
                jpnChbValues);
        
        choiceBoxJpn.getItems().addAll(jpnChbValuesObservable);
        
        choiceBoxJpn.getSelectionModel().select(KanaTypes.HIRAGANA);
        
        choiceBoxJpn.getSelectionModel()
                .selectedIndexProperty()
                .addListener( (observableValue, oldVlaue, newValue) ->  {
                    
                    ObservableList<KanaTypes> kanaTypeList = choiceBoxJpn.getItems();
                    fromJpnChoiceBoxEnum = kanaTypeList.get(newValue.intValue());
                });
        
        List<KanaTypes> latinChbValues = new ArrayList<>(EnumSet.allOf(KanaTypes.class));
        
        ObservableList<KanaTypes> latinChbValuesObservable = new ObservableListWrapper<>(
                latinChbValues);
        
        choiceBoxLatin.getItems().addAll(latinChbValuesObservable);
        
        choiceBoxLatin.getSelectionModel().select(KanaTypes.HIRAGANA);
        
        choiceBoxLatin.getSelectionModel()
                .selectedIndexProperty()
                .addListener((observableValue, oldVlaue, newValue) ->  {
                    
                    ObservableList<KanaTypes> kanaTypeList = choiceBoxLatin.getItems();
                    fromLatinChoiceBoxEnum = kanaTypeList.get(newValue.intValue());
                });
    }    
}
